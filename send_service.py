import time
import requests
import logging
import configparser
from datetime import datetime
from flask_restful import Api, Resource
from peewee import *
from flask import Flask

# Периодичность опроса
SLEEP_TIME = 1

config = configparser.RawConfigParser()
config.read('application.properties')

API_KEY = config.get('APISection', 'api.key')
API_URL = config.get('APISection', 'api.url')

DB_NAME = config.get('DatabaseSection', 'database.dbname')
DB_LOGIN = config.get('DatabaseSection', 'database.user')
DB_PASS = config.get('DatabaseSection', 'database.password')
DB_PORT = config.get('DatabaseSection', 'database.port')
DB_HOST = config.get('DatabaseSection', 'database.host')


logging.basicConfig(filename='scheduler.log', level=logging.ERROR, format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

app = Flask(__name__)
api = Api(app)

db = PostgresqlDatabase(
    DB_NAME,
    user = DB_LOGIN,
    password = DB_PASS,
    host = DB_HOST,
    port = DB_PORT)

class Newsletter(Model, Resource):
    start_date_time = DateTimeField(default=datetime.now, null = False)
    message_text = TextField(null = False)
    tag = CharField(null = False)
    end_date_time = DateTimeField(default=datetime.now, null = False)

    class Meta:
        database = db

    def serialize(self):
        return {
            'start_date_time': self.start_date_time,
            'message_text': self.message_text,
            'tag': self.tag,
            'end_date_time': self.end_date_time,
        }

class Client(Model, Resource):
    phone_number = CharField(null = False)
    code = CharField(null = False)
    tag = CharField(null = False)
    time_zone = CharField(null = False)

    class Meta:
        database = db

    def serialize(self):
        return {
            'phone_number': self.phone_number,
            'code': self.code,
            'tag': self.tag,
            'time_zone': self.time_zone,
        }

class Message(Model, Resource):
    create_date_time = DateTimeField(default=datetime.now, null = False)
    status = CharField(null = False)
    newsletter_id = ForeignKeyField(Newsletter, backref='newsletters')
    client_id = ForeignKeyField(Client, backref='clients')

    class Meta:
        database = db

    def serialize(self):
        return {
            'create_date_time': self.create_date_time,
            'status': self.status,
            'newsletter_id': self.newsletter_id,
            'client_id': self.client_id,
        }
class MessageToSend(Resource):
    def __init__(self, message_id, phone, text):
        self.message_id = message_id
        self.phone = phone
        self.text = text

    def serialize(self):
        return {
            'id': self.message_id,
            'phone': self.phone,
            'text': self.text,

        }
# Функция отправки запроса сервису отправки сообщений
def sendRequest(message_id, data):
    url = f'{API_URL}{message_id}'
    headers = {
        'Authorization': API_KEY}
    try:
        response = requests.post(url=url, headers=headers, json=data)
        app.logger.info(f'response status_code - {response.status_code}')

        return response

    except Exception as e:
        app.logger.error(f'str(e)')

db.connect()

# Основной цикл
while True:
    time.sleep(SLEEP_TIME)
    messagesToSend = Message.select().where(Message.status != 'sent')

    for messageToSend in messagesToSend:
        clientsToSend = Client.select().where(Client.id == messageToSend.client_id)
        newsLettersToSend = Newsletter.select(Newsletter.id == messageToSend.newsletter_id)
        try:
            for clientToSend in clientsToSend:

                newsletter = Newsletter.get(Newsletter.id == messageToSend.newsletter_id)
                messageToSending = MessageToSend(messageToSend.id, clientToSend.code + clientToSend.phone_number,
                                                 newsletter.message_text)
                response = sendRequest(messageToSending.message_id, messageToSending.serialize())

                try:
                    if response.status_code == 200:
                        messageToSend.status = 'sent'
                        messageToSend.save()
                        app.logger.info(f'create_newsletter : Message {messageToSend.id} sended')

                except BaseException as e:
                    app.logger.error(f'create_newsletter : {str(e)}')

        except Exception as e:
            print(str(e))