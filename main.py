import json
import logging
import requests
import configparser
import asyncio
from datetime import datetime
from aiohttp import ClientSession
from flask.json import JSONEncoder
from peewee import *
from flask import Flask, request, jsonify, redirect
from flask_restful import Api, Resource

config = configparser.RawConfigParser()
config.read('application.properties')

API_KEY = config.get('APISection', 'api.key')
API_URL = config.get('APISection', 'api.url')

DB_NAME = config.get('DatabaseSection', 'database.dbname')
DB_LOGIN = config.get('DatabaseSection', 'database.user')
DB_PASS = config.get('DatabaseSection', 'database.password')
DB_PORT = config.get('DatabaseSection', 'database.port')
DB_HOST = config.get('DatabaseSection', 'database.host')

app = Flask(__name__)
api = Api(app)

asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
logging.basicConfig(filename='api.log', level=logging.ERROR, format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

db = PostgresqlDatabase(
    DB_NAME,
    user = DB_LOGIN,
    password = DB_PASS,
    host = DB_HOST,
    port = DB_PORT)

class Newsletter(Model, Resource):
    start_date_time = DateTimeField(default=datetime.now, null = False)
    message_text = TextField(null = False)
    tag = CharField(null = False)
    end_date_time = DateTimeField(default=datetime.now, null = False)

    class Meta:
        database = db

    def serialize(self):
        return {
            'start_date_time': self.start_date_time,
            'message_text': self.message_text,
            'tag': self.tag,
            'end_date_time': self.end_date_time,
        }

class Client(Model, Resource):
    phone_number = CharField(null = False)
    code = CharField(null = False)
    tag = CharField(null = False)
    time_zone = CharField(null = False)

    class Meta:
        database = db

    def serialize(self):
        return {
            'phone_number': self.phone_number,
            'code': self.code,
            'tag': self.tag,
            'time_zone': self.time_zone,
        }

class Message(Model, Resource):
    create_date_time = DateTimeField(default=datetime.now, null = False)
    status = CharField(null = False)
    newsletter_id = ForeignKeyField(Newsletter, backref='newsletters')
    client_id = ForeignKeyField(Client, backref='clients')

    class Meta:
        database = db

    def serialize(self):
        return {
            'create_date_time': self.create_date_time,
            'status': self.status,
            'newsletter_id': self.newsletter_id,
            'client_id': self.client_id,
        }

class ErrorMessage(Resource):
    def __init__(self, error_code, text):
        self.error_code = error_code
        self.text = text

class ErrorMessageEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

class StatMessagesClass(Resource):
    def __init__(self, status, messages_count):
        self.status = status
        self.messages_count = messages_count

    def serialize(self):
        return {
            'status': self.status,
            'messages_count': self.messages_count
        }

class StatNewsletterClass(Resource):
    def __init__(self, newsletter, messages):
        self.newsletter = newsletter
        self.messages = [e.serialize() for e in messages]

    def serialize(self):
        return {
            'newsletter_id': self.newsletter.id,
            'start_date_time': self.newsletter.start_date_time,
            'message_text': self.newsletter.message_text,
            'tag': self.newsletter.tag,
            'end_date_time': self.newsletter.end_date_time,
            'messages': self.messages
        }

class DetaliseNewsletterClass(Resource):
    def __init__(self, newsletter_id, sent_messages_count):
        self.newsletter_id = newsletter_id
        self.sent_messages_count = sent_messages_count

    def serialize(self):
        return {
            'newsletter_id': self.newsletter_id,
            'sent_messages_count': self.sent_messages_count
        }

class MessageToSend(Resource):
    def __init__(self, message_id, phone, text):
        self.message_id = message_id
        self.phone = phone
        self.text = text

    def serialize(self):
        return {
            'id': self.message_id,
            'phone': self.phone,
            'text': self.text,

        }

async def sendRequest(session, message_id, data):
    url = f'{API_URL}{message_id}'
    headers = {
        'Authorization': API_KEY}
    response = await session.post(url=url, headers=headers, json=data)
    app.logger.info(f'sendRequest : response status_code - {response.status_code}')
    return {'url': response.url, 'status': response.status}


# Функция отправки асинхронного запроса сервису отправки сообщений
async def sendAsyncRequest(message_id, data):

    async with ClientSession() as session:
        task = asyncio.create_task(sendRequest(session, message_id, data))
        results = await asyncio.gather(task)

    return results

'''
# Функция отправки синхронного запроса сервису отправки сообщений
def sendRequest(message_id, data):
    url = f'{API_URL}{message_id}'
    headers = {
        'Authorization': API_KEY}
    try:
        response = requests.post(url=url, headers=headers, json=data)
        app.logger.info(f'sendRequest : response status_code - {response.status_code}')

        return response

    except Exception as e:
        app.logger.error(f'sendRequest : {str(e)}')

'''

db.connect()

db.create_tables([Newsletter, Client, Message])

# Редирект на Swagger
@app.route('/docs', methods = ['GET'])
def get_docs():
    return redirect('https://app.swaggerhub.com/apis/OreNot/test_api/1.0.0')

# Проверка работоспособности
@app.route('/')
def index():
    return json.dumps({'api_status': 'started'})

# Добавление клиента
@app.route('/addclient', methods=['POST'])
def create_client():
    record = json.loads(request.data)
    request_phone = record['phone_number']
    request_code = record['code']
    request_tag = record['tag']
    request_time_zone = record['time_zone']

    try:
        client = Client.get(Client.phone_number == request_phone, Client.code == request_code)
        app.logger.info(f'create_client : Client with phone {request_phone} and code {request_code} already exists')

        return jsonify(ErrorMessageEncoder().encode(ErrorMessage(101, f'Client with phone {request_phone} and code {request_code} already exists')))

    except BaseException:
        app.logger.info(f'create_client : Client with phone {request_phone} and code {request_code} is not exists and will be create')
        new_client = Client(phone_number = request_phone,
                            code = request_code,
                            tag = request_tag,
                            time_zone = request_time_zone)
        new_client.save()

        return jsonify(new_client.serialize())

# Обновление клиента
@app.route('/updateclient', methods=['PUT'])
def update_client():
    client_id = request.args.get('id')
    try:
        client = Client.get(Client.id == client_id)
        record = json.loads(request.data)
        client.phone_number = record['phone_number']
        client.code = record['code']
        client.tag = record['tag']
        client.time_zone = record['time_zone']

        client.save()
        app.logger.info(f'update_client: Client with id {client_id} exists and updated')

        return jsonify(client.serialize())

    except BaseException:
        app.logger.error(f'update_client: Client with id {client_id} is not exists')

        return jsonify(ErrorMessageEncoder().encode(
            ErrorMessage(102, f'Client with id {client_id} is not exists')))

# Удаление клиента
@app.route('/deleteclient', methods=['DELETE'])
def delete_client():
    client_id = request.args.get('id')
    try:
        client = Client.get(Client.id == client_id)
        client.delete_instance()
        app.logger.info(f'delete_client : Client with id {client_id} deleted')

        return jsonify(ErrorMessageEncoder().encode(
            ErrorMessage(103, f'Client with id {client_id} deleted')))

    except BaseException:
        app.logger.error(f'delete_client : Client with id {client_id} is not exists')

        return jsonify(ErrorMessageEncoder().encode(
            ErrorMessage(102, f'Client with id {client_id} is not exists')))

# Добавление новой рассылки
@app.route('/addnewsletter', methods=['POST'])
def create_newsletter():
    record = json.loads(request.data)
    request_start_date_time = datetime.strptime(record['start_date_time'], '%Y-%m-%d')
    request_message_text = record['message_text']
    request_tag = record['tag']
    request_end_date_time = datetime.strptime(record['end_date_time'], '%Y-%m-%d')

    try:
        newsletter = Newsletter.get(Newsletter.start_date_time == request_start_date_time,
                                    Newsletter.tag == request_tag,
                                    Newsletter.message_text == request_message_text,
                                    Newsletter.end_date_time == request_end_date_time,
                                    )
        app.logger.error(f'create_newsletter : Newsletter already exists')

        return jsonify(ErrorMessageEncoder().encode(ErrorMessage(104, f'Newsletter already exists')))

    except BaseException:
        new_newsletter = Newsletter(start_date_time = request_start_date_time,
                            tag = request_tag,
                            message_text = request_message_text,
                            end_date_time = request_end_date_time)

        new_newsletter.save()
        app.logger.info(f'create_newsletter : Newsletter {new_newsletter.id} created')

        currentDateTime = datetime.now()

        if currentDateTime > new_newsletter.start_date_time and currentDateTime < new_newsletter.end_date_time:

            clientsToSend = Client.select().where(Client.tag == new_newsletter.tag)
            messagesToSend = []

            for clientToSend in clientsToSend:
                new_message = Message()
                new_message.create_date_time = currentDateTime
                new_message.status = "new"
                new_message.client_id = clientToSend.id
                new_message.newsletter_id = new_newsletter.id

                new_message.save()
                app.logger.info(f'create_newsletter : Message {new_message.id} created')
                messagesToSend.append(new_message)

            for messageToSend in messagesToSend:
                try:
                    #sending
                    for clientToSend in clientsToSend:

                        newsletter = Newsletter.get(Newsletter.id == messageToSend.newsletter_id)
                        messageToSending = MessageToSend(messageToSend.id, clientToSend.code + clientToSend.phone_number, newsletter.message_text)
                        #response = sendRequest(messageToSending.message_id, messageToSending.serialize())  - Синхронный запрос
                        response = asyncio.run(sendAsyncRequest(messageToSending.message_id, messageToSending.serialize()))

                        try:
                            if response[0].get('status') == 200:
                                    messageToSend.status = 'sent'
                                    messageToSend.save()
                                    app.logger.info(f'create_newsletter : Message {messageToSend.id} sended')

                        except BaseException as e:
                            app.logger.error(f'create_newsletter : {str(e)}')

                except Exception as e:
                    app.logger.error(f'create_newsletter : {str(e)}')

        return jsonify(new_newsletter.serialize())

# Обновление рассылки
@app.route('/updatenewsletter', methods=['PUT'])
def update_newsletter():
    newsletter_id = request.args.get('id')
    try:
        newsletter = Newsletter.get(Newsletter.id == newsletter_id)
        record = json.loads(request.data)
        newsletter.start_date_time = datetime.strptime(record['start_date_time'], '%Y-%m-%d')
        newsletter.message_text = record['message_text']
        newsletter.tag = record['tag']
        newsletter.end_date_time = datetime.strptime(record['end_date_time'], '%Y-%m-%d')

        newsletter.save()
        app.logger.info(f'update_newsletter : newsletter {newsletter.id} updated')

        return jsonify(newsletter.serialize())

    except BaseException:
        app.logger.error(f'update_newsletter : Newsletter with id {newsletter_id} is not exists')

        return jsonify(ErrorMessageEncoder().encode(
            ErrorMessage(105, f'Newsletter with id {newsletter_id} is not exists')))

# Удаление рассылки
@app.route('/deletenewsletter', methods=['DELETE'])
def delete_newsletter():
    newsletter_id = request.args.get('id')
    try:
        newsletter = Newsletter.get(Newsletter.id == newsletter_id)
        newsletter.delete_instance()
        app.logger.info(f'delete_newsletter : Newsletter with id {newsletter_id} deleted')

        return jsonify(ErrorMessageEncoder().encode(
            ErrorMessage(107, f'Newsletter with id {newsletter_id} deleted')))

    except BaseException:
        app.logger.error(f'delete_newsletter : Newsletter with id {newsletter_id} is not exists')

        return jsonify(ErrorMessageEncoder().encode(
            ErrorMessage(106, f'Newsletter with id {newsletter_id} is not exists')))

# Получение статистики по рассылке
@app.route('/getstatbynewsletter', methods=['GET'])
def get_stat_by_newsletter():
    newsletter_id = request.args.get('id')
    try:
        newsletter = Newsletter.get(Newsletter.id == newsletter_id)
        try:
            sent_messages_count = Message.select().where((Message.newsletter_id == newsletter_id) & (Message.status == "sent")).count()
            app.logger.info(f'get_stat_by_newsletter : Stats by newsletter {id} ready')

            return jsonify(DetaliseNewsletterClass(newsletter_id, sent_messages_count).serialize())

        except BaseException:
            app.logger.error(f'get_stat_by_newsletter : Newsletter with id {newsletter_id} does not have messages with status sent')

            return jsonify(ErrorMessageEncoder().encode(
            ErrorMessage(109, f'Newsletter with id {newsletter_id} does not have messages with status sent')))

    except BaseException:
        app.logger.error(f'get_stat_by_newsletter : Newsletter with id {newsletter_id} is not exists')

        return jsonify(ErrorMessageEncoder().encode(
            ErrorMessage(106, f'Newsletter with id {newsletter_id} is not exists')))

# Получение статистики по всем рассылкам
@app.route('/getstat', methods=['GET'])
def get_stat():
    result = ''
    try:
        newsletters = Newsletter.select()
        message_statuses_set = set()

        messageStatuses = Message.select(Message.status).distinct()
        for item in messageStatuses:
            message_statuses_set.add(item.status)

        newsletterStatsList = []
        for newsletter in newsletters:
            statusMessagesList = []
            for status in message_statuses_set:
                try:
                    count = Message.select().where((Message.newsletter_id == newsletter.id) & (Message.status == status)).count()
                    statusMessagesList.append(StatMessagesClass(status, count))

                except BaseException:
                        app.logger.info(f'getstat : Messages for Newsletter {newsletter.id} is empty')

                        return jsonify(ErrorMessageEncoder().encode(
                            ErrorMessage(109, f'Messages for Newsletter {newsletter.id} is empty')))

            statNewsletter = StatNewsletterClass(newsletter, statusMessagesList)
            newsletterStatsList.append(statNewsletter)

        app.logger.info(f'getstat : Stats ready')

        return jsonify(newsletters = [newsletter.serialize() for newsletter in newsletterStatsList])

    except BaseException:
        app.logger.info(f'getstat : Newsletters is empty')

        return jsonify(ErrorMessageEncoder().encode(
            ErrorMessage(106, f'Newsletters is empty')))

app.run()
